<?php

namespace Tests\Unit;

use HUC\Domains\Specialties\Specialty;
use Tests\TestCase;

class SpecialtyTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testClassExists()
    {
        $this->assertTrue(class_exists(\HUC\Domains\Specialties\Specialty::class));
    }
}
