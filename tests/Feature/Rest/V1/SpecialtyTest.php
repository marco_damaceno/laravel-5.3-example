<?php

namespace Test\Feature\Rest\V1;

use HUC\Domains\Specialties\Specialty;
use Tests\TestCase;

class SpecialtyTest extends TestCase
{
    public function testCreateAnSpecialty()
    {
        $data = factory(Specialty::class)->make();

        $this->post('rest/v1/specialties', $data->toArray(), ['accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJsonStructure(array_keys($data->toArray()), $data->toArray());
    }

    public function testCreateWithInvalidData()
    {
        $data = factory(Specialty::class)->make([
            'name' => null,
        ]);

        $this->post('rest/v1/specialties', $data->toArray(), ['accept' => 'application/json'])
            ->assertStatus(422);
    }

    public function testUpdateAnSpecialty()
    {
        $specialty = factory(Specialty::class)->create();

        $r = $this->patch('rest/v1/specialties/' . $specialty->id, ['name' => 'Cardiologia'], ['accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJsonFragment([
                'id' => $specialty->id,
                'name' => 'Cardiologia',
            ]);
    }

    public function testUpdateSpecialtyWithInvalidData()
    {
        $specialty = factory(Specialty::class)->create();

        $this->patch('rest/v1/specialties/' . $specialty->id, ['name' => null], ['accept' => 'application/json'])
            ->assertStatus(422);
    }

    public function testDestroyASpecialty()
    {
        $specialty = factory(Specialty::class)->create();

        $this->delete('rest/v1/specialties/' . $specialty->id, ['accept' => 'application/json'])
            ->assertStatus(204);
    }
}
