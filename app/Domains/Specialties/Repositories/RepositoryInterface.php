<?php

namespace HUC\Domains\Specialties\Repositories;

interface RepositoryInterface
{
    public function create(array $attrs);

    public function update(array $attrs, $id);

    public function destroy($id);
}
