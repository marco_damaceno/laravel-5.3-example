<?php

namespace HUC\Domains\Specialties\Repositories;

use HUC\Domains\Specialties\Specialty;

class SpecialtyRepository implements RepositoryInterface
{
    private $specialtyModel;

    public function __construct(Specialty $specialtyModel)
    {
        $this->specialtyModel = $specialtyModel;
    }

    public function create(array $attrs)
    {
        return $this->specialtyModel->create($attrs);
    }

    public function update(array $attrs, $id)
    {
        $entity = $this->specialtyModel->find($id);

        if ($entity && $entity->update($attrs)) {
            return $this->specialtyModel->find($id);
        }

        return false;
    }

    public function destroy($id)
    {
        if ($this->specialtyModel->destroy($id)) {
            return true;
        }

        return false;
    }
}
