<?php

namespace HUC\Domains\Specialties\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            'HUC\Domains\Specialties\Repositories\RepositoryInterface',
            'HUC\Domains\Specialties\Repositories\SpecialtyRepository'
        );
    }
}
