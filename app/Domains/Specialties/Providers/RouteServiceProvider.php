<?php

namespace HUC\Domains\Specialties\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerRoutes($this->app['router']);
    }

    protected function registerRoutes(Router $router)
    {
        $router->group([
            'prefix' => 'rest/v1',
            'namespace' => 'HUC\Domains\Specialties\Http\Controllers\Rest\V1',
            'middleware' => ['api'],
        ], function ($router) {
            require app_path('Domains/Specialties/Http/routes.php');
        });
    }
}
