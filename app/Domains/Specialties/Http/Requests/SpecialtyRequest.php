<?php

namespace HUC\Domains\Specialties\Http\Requests;

use HUC\Domains\BaseFormRequest;

class SpecialtyRequest extends BaseFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
                return [];
                break;

            case 'POST':
                return [
                    'name' => 'required',
                ];
                break;

            case 'PUT':
            case 'PATCH':
                return [
                    'name' => 'sometimes|required',
                ];
                break;

            case 'DELETE':
                return [];
                break;
        }
    }
}
