<?php

namespace HUC\Domains\Specialties\Http\Controllers\Rest\V1;

use HUC\Domains\Specialties\Http\Requests\SpecialtyRequest;
use HUC\Domains\Specialties\Repositories\SpecialtyRepository;

class SpecialtiesController
{
    public function __construct(SpecialtyRepository $specialtyRepo)
    {
        $this->specialtyRepo = $specialtyRepo;
    }

    public function store(SpecialtyRequest $request)
    {
        $specialty = $this->specialtyRepo->create($request->all());

        return response()->json($specialty, 201);
    }

    public function update(SpecialtyRequest $request, $id)
    {
        $specialty = $this->specialtyRepo->update($request->all(), $id);

        return response()->json($specialty, 201);
    }

    public function destroy($id)
    {
        $this->specialtyRepo->destroy($id);

        return response()->json([], 204);
    }
}
