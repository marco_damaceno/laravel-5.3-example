<?php

namespace HUC\Domains\Specialties;

use Eloquence\Behaviours\Uuid;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Specialty extends Eloquent
{
    use Uuid;

    public $incrementing = false;

    protected $table = 'specialties';

    protected $fillable = [
        'name',
    ];
}
