#/bin/bash

docker rm -f HUCPostgresTest

# docker run --rm --name HUCPostgresDev \
# -e POSTGRES_USER=HUC \
# -e POSTGRES_PASSWORD=HUC \
# -e POSTGRES_DB=HUC \
# -e PGDATA=/var/lib/postgresql/data/pgdata \
# -v $(pwd)/data/postgres:/var/lib/postgresql/data/pgdata \
# -p 5460:5432 \
# postgres:9.6

docker run -d --name HUCPostgresTest \
-e POSTGRES_USER=HUC \
-e POSTGRES_PASSWORD=HUC \
-e POSTGRES_DB=HUC \
-e PGDATA=/var/lib/postgresql/test/data/pgdata \
-v $(pwd)/data/postgres:/var/lib/postgresql/test/data/pgdata \
-p 5460:5432 \
postgres:9.6
